import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        int max = 0;
        int iMax = -1;
        for (int i = 0; i < ships.size(); i++) {
            if (ships.get(i).getFirePower() > max) {
                max = ships.get(i).getFirePower();
                iMax = i;
            }
        }
        if (iMax == -1) {
            return null;
        } else return ships.get(iMax);
    }

    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for (int i = 0; i < ships.size(); i++) {
            if (ships.get(i).getName().equals(name)) {
                return ships.get(i);
            }
        }
        return null;
    }

    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> spaceshipsWithEnoughCargoSpace = new ArrayList<>();
        for (int i = 0; i < ships.size(); i++) {
            if (ships.get(i).getCargoSpace() >= cargoSize) {
                spaceshipsWithEnoughCargoSpace.add(ships.get(i));
            }
        }
        return spaceshipsWithEnoughCargoSpace;
    }

    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> allCivilianShips = new ArrayList<>();
        for (int i = 0; i < ships.size(); i++) {
            if (ships.get(i).getFirePower() == 0) {
                allCivilianShips.add(ships.get(i));
            }
        }
        return allCivilianShips;
    }
}

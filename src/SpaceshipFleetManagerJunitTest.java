import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class SpaceshipFleetManagerJunitTest {
    SpaceshipFleetManager center = new CommandCenter();
    ArrayList<Spaceship> testList = new ArrayList<>();
    ArrayList<Spaceship> result = new ArrayList<>();

    @BeforeEach
    void clearArrayLists() {
        testList.clear();
        result.clear();
    }

    @Test
    void getMostPowerfulShip_returnTargetSpaceships() {
        Spaceship spaceship1 = new Spaceship("Mark", 10, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 5, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 5, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        Spaceship mostPowerfulShip = center.getMostPowerfulShip(testList);
        Assertions.assertEquals(10, mostPowerfulShip.getFirePower());
    }

    @Test
    void getMostPowerfulShip_returnNull() {
        Spaceship spaceship1 = new Spaceship("Mark", 0, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 0, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 0, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        Spaceship mostPowerfulShip = center.getMostPowerfulShip(testList);
        Assertions.assertNull(mostPowerfulShip);
    }

    @Test
    void getShipByName_returnTargetShip() {
        Spaceship spaceship1 = new Spaceship("Mark", 0, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 0, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 0, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        String name = "Mark";
        Spaceship shipByName = center.getShipByName(testList, name);
        Assertions.assertEquals(name, shipByName.getName());
    }

    @Test
    void getShipByName_returnNull() {
        Spaceship spaceship1 = new Spaceship("Mark", 0, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 0, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 0, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        String name = "Mark IV";
        Spaceship shipByName = center.getShipByName(testList, name);
        Assertions.assertNull(shipByName);
    }

    @Test
    void getAllShipsWithEnoughCargoSpace_returnTargetSpaceships() {
        Spaceship spaceship1 = new Spaceship("Mark", 5, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 5, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 5, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        int cargoSapce = 3;
        result = center.getAllShipsWithEnoughCargoSpace(testList, cargoSapce);
        for (int i = 0; i < result.size() ; i++) {
            Assertions.assertTrue(result.get(i).getFirePower() > cargoSapce);
        }
    }

    @Test
    void getAllShipsWithEnoughCargoSpace_returnClearArrayList() {
        Spaceship spaceship1 = new Spaceship("Mark", 5, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 5, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 5, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        int cargoSapce = 20;
        result = center.getAllShipsWithEnoughCargoSpace(testList, cargoSapce);
        for (int i = 0; i < result.size() ; i++) {
            Assertions.assertTrue(result.get(i).getCargoSpace() > cargoSapce);
        }
    }

    @Test
    void getAllCivilianShips_returnTargetShips() {
        Spaceship spaceship1 = new Spaceship("Mark", 0, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 5, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 5, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        result = center.getAllCivilianShips(testList);
        for (int i = 0; i < result.size() ; i++) {
            Assertions.assertTrue(result.get(i).getFirePower() == 0);
        }
    }

    @Test
    void getAllCivilianShips_returnClearList() {
        Spaceship spaceship1 = new Spaceship("Mark", 10, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 5, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 5, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        result = center.getAllCivilianShips(testList);
        for (int i = 0; i < result.size() ; i++) {
            Assertions.assertTrue(result.get(i).getFirePower() == 0);
        }
    }
}
import java.util.ArrayList;

public class SpaceshipFleetManagerTest {

    static SpaceshipFleetManager center = new CommandCenter();
    static ArrayList<Spaceship> testList = new ArrayList<>();
    static ArrayList<Spaceship> result = new ArrayList<>();


    public static void main(String[] args) {
        double points = 0;
        if (getMostPowerfulShip_returnTargetSpaceships()) {
            System.out.println("All good");
            points += 0.5;
        } else {
            System.out.println("Bad");
        }

        if (getMostPowerfulShip_returnNull()) {
            System.out.println("All good");
            points += 0.5;
        } else {
            System.out.println("Bad");
        }

        if (getShipByName_returnTargetShip()) {
            System.out.println("All good");
            points += 0.5;
        } else {
            System.out.println("Bad");
        }

        if (getShipByName_returnNull()) {
            System.out.println("All good");
            points += 0.5;
        } else {
            System.out.println("Bad");
        }

        if (getAllShipsWithEnoughCargoSpace_returnTargetSpaceships()) {
            System.out.println("All good");
            points += 0.5;
        } else {
            System.out.println("Bad");
        }

        if (getAllShipsWithEnoughCargoSpace_returnClearArrayList()) {
            System.out.println("All good");
            points += 0.5;
        } else {
            System.out.println("Bad");
        }

        if (getAllCivilianShips_returnTargetShips()) {
            System.out.println("All good");
            points += 0.5;
        } else {
            System.out.println("Bad");
        }

        if (getAllCivilianShips_returnClearList()) {
            System.out.println("All good");
            points += 0.5;
        } else {
            System.out.println("Bad");
        }
        System.out.println("Points: " + points);
    }

    static boolean getMostPowerfulShip_returnTargetSpaceships() {
        testList.clear();
        result.clear();
        Spaceship spaceship1 = new Spaceship("Mark", 10, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 5, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 5, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        Spaceship mostPowerfulShip = center.getMostPowerfulShip(testList);
        for (int i = 0; i < testList.size() ; i++) {
            if (mostPowerfulShip.getFirePower() < testList.get(i).getFirePower()) {
                return false;
            }
        }
        return true;
    }

    static boolean getMostPowerfulShip_returnNull() {
        testList.clear();
        result.clear();
        Spaceship spaceship1 = new Spaceship("Mark", 0, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 0, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 0, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        Spaceship mostPowerfulShip = center.getMostPowerfulShip(testList);
        if (mostPowerfulShip == null) {
            return true;
        }
        return false;
    }

    static boolean getShipByName_returnTargetShip() {
        testList.clear();
        result.clear();
        Spaceship spaceship1 = new Spaceship("Mark", 0, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 0, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 0, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        String name = "Mark";
        Spaceship shipByName = center.getShipByName(testList, name);
        if (shipByName.getName().equals(name)) {
            return true;
        }
        return false;
    }

    static boolean getShipByName_returnNull() {
        testList.clear();
        result.clear();
        Spaceship spaceship1 = new Spaceship("Mark", 0, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 0, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 0, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        String name = "Mark IV";
        Spaceship shipByName = center.getShipByName(testList, name);
        if (shipByName == null) {
            return true;
        }
        return false;
    }

    static boolean getAllShipsWithEnoughCargoSpace_returnTargetSpaceships() {
        testList.clear();
        result.clear();
        Spaceship spaceship1 = new Spaceship("Mark", 5, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 5, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 5, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        int cargoSapce = 3;
        result = center.getAllShipsWithEnoughCargoSpace(testList, cargoSapce);
        for (int i = 0; i < result.size() ; i++) {
            if (result.get(i).getCargoSpace() < cargoSapce) {
                return false;
            }
        }
        return true;
    }

    static boolean getAllShipsWithEnoughCargoSpace_returnClearArrayList() {
        testList.clear();
        result.clear();
        Spaceship spaceship1 = new Spaceship("Mark", 5, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 5, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 5, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        int cargoSapce = 20;
        result = center.getAllShipsWithEnoughCargoSpace(testList, cargoSapce);
        for (int i = 0; i < result.size() ; i++) {
            if (result.get(i).getCargoSpace() < cargoSapce) {
                return false;
            }
        }
        return true;
    }


    static boolean getAllCivilianShips_returnTargetShips() {
        testList.clear();
        result.clear();
        Spaceship spaceship1 = new Spaceship("Mark", 0, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 5, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 5, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        result = center.getAllCivilianShips(testList);
        for (int i = 0; i < result.size() ; i++) {
            if (result.get(i).getFirePower() != 0) {
                return false;
            }
        }
        return true;
    }

    static boolean getAllCivilianShips_returnClearList() {
        testList.clear();
        result.clear();
        Spaceship spaceship1 = new Spaceship("Mark", 10, 12, 2);
        Spaceship spaceship2 = new Spaceship("Mark II", 5, 5, 2);
        Spaceship spaceship3 = new Spaceship("Mark III", 5, 2, 2);
        testList.add(spaceship1);
        testList.add(spaceship2);
        testList.add(spaceship3);
        result = center.getAllCivilianShips(testList);
        for (int i = 0; i < result.size() ; i++) {
            if (result.get(i).getFirePower() != 0) {
                return false;
            }
        }
        return true;
    }
}